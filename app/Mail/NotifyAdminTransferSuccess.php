<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyAdminTransferSuccess extends Mailable
{
    use Queueable, SerializesModels;

    public $transfer;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($transfer,$to)
    {
        $this->transfer = $transfer;
        $this->user = $to;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.admin.success-transfer');
    }
}
