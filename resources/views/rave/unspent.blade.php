@extends('layouts.app')

@section('title') Unspent Bitcoin Transaction | {{ config('app.name', 'eCurrencyNG') }} @endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('includes.flash')
            <div class="card">
                <div class="card-header">{{ __('Payment Confirmation') }}</div>
                <div class="card-body">
                	<p class="text text-success">Your trasaction would be unspent that would be confirm after 3 confirmation.So till that wait when transaction would be confirmed we will update your trsaction status.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection