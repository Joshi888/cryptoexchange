@extends('admin.layouts.layout')

@section('title') Transaction | {{ config('app.name', 'eCurrencyNG') }} @endsection

@section('content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		@include('admin.includes.flash')
		<div class="card">
			<div class="card-header">
				<h5>Transaction List</h5>
				<span></span>
			</div>
			<div class="card-block">
				<div class="dt-responsive table-responsive">
					<!-- <a href="{{ route('admin.transaction.create') }}" class="btn btn-primary btn-sm pull-right">Add New<i
							class="ti-plus"></i></a> -->
					<table id="transactionTable" class="table table-striped table-bordered nowrap">
						<thead>
							<tr>
								<th>Sr No.</th>
								<th>Name</th>
								<th>Amount</th>
								<th>Transfer</th>
								<th>Txn Status</th>
								<th>Transfer Status</th>
								<th>Created At</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@if(isset($data) && count($data) > 0)
								@foreach($data as $key=>$val)
									@if($val['rave']->txn_status == "1")
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{ $val["user"]->name }}</td>
										<td>{{ ($val->amount === "" ?? 0) - ($val["rave"]->charges === "" ?? 0 )." ".$val->from_currency }}</td>
										<td>{{ $val->transferAmount }} {{ $val->to_currency }}</td>
										<td>
											@if($val["rave"]->txn_status == "1") 
											<span class="label label-success">{{ __('Success') }}</span> 
											@else 
											<span class="label label-warning">{{ __('Pending') }}</span>
											@endif
										</td>
										<td>
											@csrf
											@if($val->status == "0")
												<span class="label label-warning">{{ __('Pending') }}</span>
												<!-- <button type="button" name="status" id="status" class="btn btn-warning btn-sm" data-id={{ $val->id }} data-value="{{ $val->status }}"><i class="ti-info"></i>{{ __('Pending') }}</button> -->
											@elseif($val->status == "1")
												<span class="label label-success">{{ __('Success') }}</span>
												<!-- <button type="button" name="status" id="status" class="btn btn-success btn-sm" data-id={{ $val->id }} data-value="{{ $val->status }}"><i class="ti-check"></i>{{ __('Success') }}</button> -->
											@else
												<span class="label label-danger">{{ __('Reject') }}</span>
												<!-- <button type="button" name="status" id="status" class="btn btn-danger btn-sm" data-id={{ $val->id }}  data-value="{{ $val->status }}"><i class="ti-close"></i>{{ __('Reject') }}</button> -->
											@endif
										</td>
										<td>{{ Carbon::parse($val->created_at)->format('Y-m-d h:i A') }}</td>
										<td>
											@if($val->status == "0")
												<button type="button" name="status" onclick="autoTransferAdmin(this);" class="btn btn-success btn-sm" data-to={{ $val->to_currency }} data-id={{ $val->id }}  data-value="{{ $val->status }}" data-status="success"><i class="ti-check"></i></button>
												<button type="button" name="status" id="reject" class="btn btn-danger btn-sm" data-id={{ $val->id }}  data-value="{{ $val->status }}" data-status="reject"><i class="ti-close"></i></button>
											@elseif($val->status == "1")
												<span class="label label-primary ">{{ __('Approved') }}</span>
											@else
												<span class="label label-danger">{{ __('Rejected') }}</span>
											@endif
										</td>
										<!-- <td>
											<a href="{{ route('admin.transaction.edit',$val['id']) }}"
												class="btn btn-info btn-sm"><i class="ti-pencil-alt"></i></a>
											<form id="delete-form" action="{{ route('admin.transaction.destroy',$val['id']) }}"
												method="POST">
												@method('DELETE')
												@csrf
												<button type="submit" name="delete" class="btn btn-danger btn-sm"><i
														class="ti-trash"></i></button>
											</form>
										</td> -->
									</tr>
									@endif
								@endforeach
							@else
								<tr>
									<td colspan="8" align="center">No Record Found</td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
@endsection